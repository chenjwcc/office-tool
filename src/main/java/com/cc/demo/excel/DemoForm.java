package com.cc.demo.excel;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.cc.excel.annotaion.*;
import com.cc.excel.model.ExcelEnum;

public class DemoForm implements Serializable{

	private static final long serialVersionUID = 1L;

	@ExcelField(name = "合同号",  isRequire = false)
	@ExcelValid(regexp = "\\d{6}$")
	private String contractNo;
	
	@ExcelField(name = "客户名称", isRequire = false)
	//@ExcelCustomData(className = "com.cc.demo.excel.DemoFunction", functionName = "getClientName", message = "客户不存在")
	//@ExcelFunction(className = "com.cc.demo.excel.DemoFunction", functionName = "valilClientName")
	private String clientName;
	
	@ExcelField(name = "申请金额", isRequire = false)
	@ExcelValid(regexp = "^[+-]?[0-9]+(.[0-9]{1,2})?$")
	private BigDecimal applySum;
	
	@ExcelField(name = "费用", isRequire = false)
	@ExcelBigDecimal(scale = 2)
	private BigDecimal fee;
	
	@ExcelField(name = "合同起始日期")
	@ExcelDate
	private Date contractBeginDate;
	
	@ExcelField(name = "合同终止日期", sort = 5)
	@ExcelDate(format = {"yyyy/MM/dd","yyyy年MM月dd日"})
	private Date contractEndDate;
	
	@ExcelField(name = "是否存量数据", sort = 6)
	@ExcelBoolean
	private Boolean isStock;

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public BigDecimal getApplySum() {
		return applySum;
	}

	public void setApplySum(BigDecimal applySum) {
		this.applySum = applySum;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Date getContractBeginDate() {
		return contractBeginDate;
	}

	public void setContractBeginDate(Date contractBeginDate) {
		this.contractBeginDate = contractBeginDate;
	}

	public Date getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(Date contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public Boolean getIsStock() {
		return isStock;
	}

	public void setIsStock(Boolean isStock) {
		this.isStock = isStock;
	}
	
}
