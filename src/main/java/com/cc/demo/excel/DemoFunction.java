package com.cc.demo.excel;

import com.cc.excel.validator.ExcelValidatorResult;

/**
 * ClassName DemoFunction
 * Author chenjw
 **/
public class DemoFunction {

    public static String[] getClientNames(){
        return new String[]{"张三","王五"};
    }

    public static ExcelValidatorResult valilClientName(String content){
        ExcelValidatorResult result = new ExcelValidatorResult();
        result.setResult(false);
        result.setMessage(content + "bufuhe\n");
        return result;
    }
}
