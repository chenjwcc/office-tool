package com.cc.demo.excel;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cc.api.ExcelApiUtil;
import com.cc.excel.validator.ExcelValidatorResult;
import com.cc.watermark.WaterMarkModel;

public class DemoTest {

	public static void main(String[] args) throws Exception {
		//importExcelDemo();
		//addWaterMarkDemo();
		exportExcel();
	}


	public static void importExcelDemo() throws Exception{
		File file = new File("E:\\project\\IdeaProjects\\java-excel\\src\\main\\java\\com\\cc\\demo\\excel\\demoExcel.xlsx");

		ExcelValidatorResult model = ExcelApiUtil.checkExcelHeader(file, DemoForm.class);
		System.out.println(model);
		if (!model.getResult()) {
			return;
		}
		//model = JxlExcelUtil.checkExcelContent(file, DemoForm.class);
		model = ExcelApiUtil.checkExcelContent(file, DemoForm.class);
		System.out.println(model);
		if (!model.getResult()) {
			return;
		}

		//List<DemoForm> demoForms = JxlExcelUtil.excelToEntity(file, DemoForm.class);
		List<DemoForm> demoForms = ExcelApiUtil.excelToEntity(file, DemoForm.class);
		demoForms.forEach(ea -> {
			System.out.println(ea.getContractNo());
			System.out.println(ea.getApplySum());
			System.out.println(ea.getContractBeginDate());
			System.out.println(ea.getIsStock());
		});
	}

	public static void addWaterMarkDemo() throws Exception{
		File file = new File("D:\\product\\测试水印.xlsx");
		File outFile = new File("D:\\product\\水印.xls");
		ExcelApiUtil.addWaterMark(file, outFile, "内部文件，不可修改");
	}

	public static void exportExcel() throws Exception{
		File file = new File("D:\\product\\导出模板.xls");
		File outFile = new File("D:\\product\\导出文件.xls");
		Map<String, Object> map = new HashMap<>();
		map.put("name", "张三");
		System.out.println(ExcelApiUtil.exportExcel(file, outFile, map));
	}

}
