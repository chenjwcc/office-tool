package com.cc.excel.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * excel字段属性
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelField {

	/**
	 * 名称
	 * @return
	 */
	String name();
	
	/**
	 * 排序
	 * @return
	 */
	int sort() default 0;
	
	/**
	 * 是否允许为空，默认为true
	 * @return
	 */
	boolean isRequire() default true;
}
