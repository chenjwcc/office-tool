package com.cc.excel.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段验证
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelValid {

	/**
	 * 正则表达式
	 * @return
	 */
	String regexp() default  "";
	
	/**
	 * 示例
	 * @return
	 */
	String example() default "";
}
