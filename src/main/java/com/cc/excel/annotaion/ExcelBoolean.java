package com.cc.excel.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.cc.excel.model.ExcelEnum;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelBoolean {

	ExcelEnum False() default ExcelEnum.FALSE;
	
	ExcelEnum True()default ExcelEnum.TRUE;
}
