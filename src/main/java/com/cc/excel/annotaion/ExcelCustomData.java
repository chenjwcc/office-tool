package com.cc.excel.annotaion;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义结果集
 * author chenjw
 *      自定义方法：返回类型为String[]的结果集
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelCustomData {

    /**
     * 类名
     * @return
     */
    String className();

    /**
     * 方法名
     * @return
     */
    String functionName();

    /**
     * 自定义属性值
     * @return
     */
    String[] customData() default {};

    /**
     * 提示语
     * @return
     */
    String message() default  "";
}
