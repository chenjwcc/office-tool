package com.cc.excel.tool;

import com.cc.excel.factory.ExcelAbstractTool;
import com.cc.excel.model.ExcelModel;
import com.cc.excel.validator.ExcelValidatorUtil;
import com.cc.watermark.WaterMarkModel;
import com.cc.watermark.WaterMarkUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePartName;
import org.apache.poi.openxml4j.opc.PackageRelationship;
import org.apache.poi.openxml4j.opc.TargetMode;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

import static org.apache.poi.ss.usermodel.CellType.NUMERIC;

/**
 * ClassName PoiExcelTool
 * Author chenjw
 **/
public class PoiExcelTool extends ExcelAbstractTool {

    @Override
    public ExcelModel readExcel(File file, Class<?> clazz, Integer sheetNumber, Integer rowNumber) throws Exception {
        ExcelModel excelModel = new ExcelModel();
        try {
            Workbook wb = createWorkbook(file);

            //表头
            Map<Integer, String> headerMap = new HashMap<>();
            List<Map<String, String>> list = new ArrayList<>();
            // 获取sheet
            Sheet sheet = wb.getSheetAt(sheetNumber);
            // 获取表头
            Row rowHeader = sheet.getRow(rowNumber);

            // 获取最大行数
            int rowNum = sheet.getPhysicalNumberOfRows();
            // 获取最大列数
            int colNum = rowHeader.getPhysicalNumberOfCells();

            for (int k = 0; k < colNum; k++) {
                String content = getCellStringValue(rowHeader.getCell(k));
                if (ExcelValidatorUtil.isEffective(content)){
                    //获取指定单元格表头的名称
                    headerMap.put(k, content);
                }
            }
            excelModel.setHeader(headerMap);

            for (int i = rowNumber + 1; i < rowNum; i++) {
                Map<String, String> map = new LinkedHashMap<String, String>();
                Row row = sheet.getRow(i);
                if (row != null) {
                    for (int j = 0; j < colNum; j++) {
                        String content = getCellStringValue(row.getCell(j));
                        //获取指定单元格表头的名称
                        String headName = headerMap.get(j);
                        map.put(headName, content);

                    }
                } else {
                    break;
                }
                list.add(map);
            }
            excelModel.setContent(list);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return excelModel;
    }

    /**	获取单个单元格数据
     * @param cell
     * @return
     */
    private String getCellStringValue(Cell cell) {
        String cellValue = null;
        if(null == cell) {
            return "";
        }
        switch (cell.getCellType()) {
            case NUMERIC: // 数字
                if (org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    cellValue = sdf.format(org.apache.poi.ss.usermodel.DateUtil.getJavaDate(cell.getNumericCellValue())).toString();
                } else {
                    DataFormatter dataFormatter = new DataFormatter();
                    cellValue = dataFormatter.formatCellValue(cell);
                }
                break;
            case STRING: // 字符串
                cellValue = cell.getStringCellValue();
                break;
            case BOOLEAN: // Boolean
                cellValue = cell.getBooleanCellValue() + "";
                break;
            case FORMULA: // 公式
                cellValue = cell.getCellFormula() + "";
                break;
            case BLANK: // 空值
                cellValue = "";
                break;
            case ERROR: // 故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

    private Workbook createWorkbook(File file) throws Exception {
        Workbook workbook = null;
        InputStream is = new FileInputStream(file);
        String extString = file.getName().substring(file.getName().lastIndexOf("."));
        if (".xls".equals(extString)) {
            workbook = new HSSFWorkbook(is);
        } else if (".xlsx".equals(extString)) {
            workbook = new XSSFWorkbook(is);
        } else {
            throw new Exception("文件类型有误");
        }
        return workbook;
    }


    /**
     * excel添加水印
     * @param file
     * @throws Exception
     */
    @Override
    public Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel) throws Exception {
        try {
            FileOutputStream out = new FileOutputStream(outFile);

            BufferedImage image = WaterMarkUtil.createWaterMark(waterMarkModel);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            ImageIO.write(image, "png", os);

            InputStream is = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            int pictureIdx = workbook.addPicture(os.toByteArray(), Workbook.PICTURE_TYPE_PNG);
            POIXMLDocumentPart poixmlDocumentPart = workbook.getAllPictures().get(pictureIdx);
            for (int i = 0; i < workbook.getNumberOfSheets(); i++) {//获取每个Sheet表
                XSSFSheet sheet = workbook.getSheetAt(i);
                PackagePartName ppn = poixmlDocumentPart.getPackagePart().getPartName();
                String relType = XSSFRelation.IMAGES.getRelation();
                //add relation from sheet to the picture data
                PackageRelationship pr = sheet.getPackagePart().addRelationship(ppn, TargetMode.INTERNAL, relType, null);
                //set background picture to sheet
                sheet.getCTWorksheet().addNewPicture().setId(pr.getId());
            }
            workbook.write(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public Boolean exportExcel(File file, File outFile, Map<String, Object> map) throws Exception {
        throw new Exception("暂不支持");
    }

}
