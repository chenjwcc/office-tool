package com.cc.excel.tool;

import com.cc.excel.factory.ExcelAbstractTool;
import com.cc.excel.model.ExcelModel;
import com.cc.excel.validator.ExcelValidatorUtil;
import com.cc.watermark.WaterMarkModel;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


/**
 * ClassName JxlExcelTool
 * Author chenjw
 **/
public class JxlExcelTool extends ExcelAbstractTool {

    /**
     * 读取excel数据
     * @param file
     * @param clazz
     * @param sheetNumber
     * @param rowNumber
     * @return
     * @throws Exception
     */
    @Override
    public ExcelModel readExcel(File file, Class<?> clazz, Integer sheetNumber, Integer rowNumber) throws Exception {

        ExcelModel excelModel = new ExcelModel();
        InputStream stream = null;
        Workbook rwb = null;
        String extString = file.getName().substring(file.getName().lastIndexOf("."));
        if (!".xls".equals(extString)) {
            throw new Exception("JXL仅支持xls文件");
        }

        try {
            stream = new FileInputStream(file);
            //获取Excel文件对象
            rwb = Workbook.getWorkbook(stream);
            //获取文件的指定工作表
            Sheet sheet = rwb.getSheet(sheetNumber);

            //获取excel内容长度
            int size = sheet.getRows();
            //获取excel表头
            Cell[] heads = sheet.getRow(rowNumber);
            Map<Integer, String> headerMap = new HashMap<>();
            for (int j = 0; j < heads.length; j++) {
                headerMap.put(j, heads[j].getContents());
            }
            excelModel.setHeader(headerMap);

            List<Map<String, String>> list = new ArrayList<>();
            for (int i = rowNumber + 1; i < size; i++) {
                Map<String, String> map = new HashMap<>();
                //获取一行的数据
                Cell[] cells = sheet.getRow(i);
                //获取行中cell数量
                int len = cells.length;
                for (int j = 0; j < len; j++) {
                    String content = cells[j].getContents();
                    if (ExcelValidatorUtil.isEffective(content)) {
                        //获取指定单元格表头的名称
                        String headName = heads[j].getContents();
                        map.put(headName, content);
                    }
                }
                list.add(map);
            }
            excelModel.setContent(list);
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (rwb != null) {
                rwb.close();
            }
        }
        return excelModel;
    }

    @Override
    public Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel) throws Exception{
        throw new Exception("不支持对xls添加水印");
    }

    @Override
    public Boolean exportExcel(File file, File outFile, Map<String, Object> map) throws Exception{
        throw new Exception("暂不支持");
    }
}
