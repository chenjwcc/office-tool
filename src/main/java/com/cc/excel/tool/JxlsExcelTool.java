package com.cc.excel.tool;

import com.cc.excel.factory.ExcelAbstractTool;
import com.cc.excel.model.ExcelModel;
import com.cc.watermark.WaterMarkModel;
import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import java.io.*;
import java.util.Map;

/**
 * ClassName JxlsExcelTool
 * Author chenjw
 **/
public class JxlsExcelTool extends ExcelAbstractTool {
    @Override
    public ExcelModel readExcel(File file, Class<?> clazz, Integer sheetNumber, Integer rowNumber) throws Exception {
        throw new Exception("暂不支持");
    }

    @Override
    public Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel) throws Exception {
        throw new Exception("暂不支持");
    }

    @Override
    public Boolean exportExcel(File file, File outFile, Map<String, Object> map) throws Exception {
        try {
            Context context = new Context();
            if (map != null) {
                for (String key : map.keySet()) {
                    context.putVar(key, map.get(key));
                }
            }
            InputStream is = new FileInputStream(file);
            OutputStream os = new FileOutputStream(outFile);
            JxlsHelper.getInstance().processTemplate(is, os, context);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
