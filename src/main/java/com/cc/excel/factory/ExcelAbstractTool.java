package com.cc.excel.factory;

import com.cc.excel.model.ExcelModel;
import com.cc.watermark.WaterMarkModel;
import java.io.File;
import java.util.Map;

/**
 * ClassName ExcelAbstractTool
 * Author chenjw
 **/
public abstract class ExcelAbstractTool {

    /**
     * excel读取
     * @return
     */
    public abstract ExcelModel readExcel(File file, Class<?> clazz, Integer sheetNumber, Integer rowNumber) throws Exception;

    /**
     * excel添加水印
     * @param file
     * @param outFile
     * @param waterMarkModel
     * @throws Exception
     */
    public abstract Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel) throws Exception;

    /**
     * 导出excel
     * @param file
     * @param map
     */
    public abstract Boolean exportExcel(File file, File outFile, Map<String, Object> map) throws Exception;
}
