package com.cc.excel.factory;

import com.cc.excel.tool.JxlExcelTool;
import com.cc.excel.model.ExcelToolEnum;
import com.cc.excel.tool.JxlsExcelTool;
import com.cc.excel.tool.PoiExcelTool;

/**
 * ClassName ExcelToolFactory
 * Author chenjw
 **/
public class ExcelToolFactory {

    public static ExcelAbstractTool build(ExcelToolEnum toolType){
        if (ExcelToolEnum.JXL.equals(toolType)) {
            return new JxlExcelTool();
        } else if (ExcelToolEnum.POI.equals(toolType)){
            return new PoiExcelTool();
        } else if (ExcelToolEnum.JXLS.equals(toolType)) {
            return new JxlsExcelTool();
        } else {
            return new PoiExcelTool();
        }

    }
}
