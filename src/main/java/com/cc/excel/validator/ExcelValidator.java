package com.cc.excel.validator;

import com.cc.excel.model.ExcelHelper;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * excel验证器
 * @author chenjw
 */
public final class ExcelValidator {

    /**
     * 验证表头
     * @param sheet
     * @return
     * @throws Exception
     */
    public static ExcelValidatorResult equalsTitleAndSort(Map<Integer, String> sheet, Class<?> clazz, Boolean isCheckSort) throws Exception{
        ExcelValidatorResult excelValidatorResult = new ExcelValidatorResult();
        Boolean result = true;
        StringBuffer message = new StringBuffer();

        Map<Integer, String> map = ExcelValidatorUtil.getBeanFieldNameAndSort(clazz);
        for (Map.Entry<Integer, String> m : sheet.entrySet()) {
            String name = m.getValue();
            if (isCheckSort && !name.equals(map.get(m.getKey()))) {
                result = false;
                message.append("【").append(name).append("】字段顺序或名称与系统不一致");
            }

            if (!isCheckSort && !map.containsValue(name)) {
                result = false;
                message.append("【").append(name).append("】字段系统不存在");
            }
        }
        excelValidatorResult.setResult(result);
        excelValidatorResult.setMessage(message.toString());
        return excelValidatorResult;
    }

    public static ExcelValidatorResult validExcelContent(List<Map<String, String>> contents, Integer rowNumber, Class<?> clazz) throws Exception {
        Boolean checkResult = true;
        StringBuilder checkMessage = new StringBuilder();

        Integer sort = rowNumber+1;
        //获取实体类中的属性字段map：字段名=ExcelHelper
        Map<String, ExcelHelper> map = ExcelValidatorUtil.loadExcelAnnotationFieldVlaue(clazz);
        for (Map<String, String> content : contents) {
            sort++;

            //如果content中都为空，则跳过
            if(checkRow(content)){continue;}

            for (Map.Entry<String, String> m : content.entrySet()) {
                //验证单元格
                ExcelValidatorResult excelValidatorResult = ExcelValidator.validCell(map, m.getKey(), m.getValue());
                if (!excelValidatorResult.getResult()) {
                    checkResult = false;
                    checkMessage.append("【行数:").append(sort).append("】");
                    checkMessage.append(excelValidatorResult.getMessage());
                }
            }
        }

        ExcelValidatorResult result = new ExcelValidatorResult();
        result.setResult(checkResult);
        result.setMessage(checkMessage.toString());
        return result;
    }

    /**
     * 验证excelCell
     * @param map
     * @param headName
     * @param fieldContent
     * @return
     */
    public static ExcelValidatorResult validCell(Map<String, ExcelHelper> map, String headName, String fieldContent) throws Exception {
        ExcelValidatorResult excelValidatorResult = new ExcelValidatorResult();
        Boolean result = true;
        StringBuilder sb = new StringBuilder();

        //根据单元格表头名称获取指定excelhelper
        ExcelHelper helper = map.get(headName);

        // 根据是否有isRequire判断字段内容是否为必填字段
        if (helper.isRequire()) {
            if (!ExcelValidatorUtil.isEffective(fieldContent)){
                result = false;
                sb.append("【").append(headName).append("】不能为空\n");
            }
        }

        // 判断字段注解是否存在规则过滤
        if (ExcelValidatorUtil.isEffective(fieldContent)) {
            if (ExcelValidatorUtil.isEffective(helper.getRegexp())) {
                if (!ExcelValidatorUtil.match(helper.getRegexp(), fieldContent)) {
                    result = false;
                    sb.append("【").append(headName).append(":").append(fieldContent).append("】格式不正确,示例格式:").append(helper.getExample()).append("\n");
                }
            }

            //自定义属性集
            if (null != helper.getCustomData() && helper.getCustomData().length > 0) {
                Boolean dateCheck = false;
                for (String cutomData : helper.getCustomData()) {
                    if (cutomData.equals(fieldContent)) {
                        dateCheck = true;
                        break;
                    }
                }
                if (!dateCheck) {
                    result = false;
                    sb.append("【").append(headName).append(":").append(fieldContent).append("】格式不正确;");
                    if (ExcelValidatorUtil.isEffective(helper.getMessage())) {
                        sb.append(helper.getMessage());
                    }
                    sb.append("\n");
                }
            }

            //自定义方法
            if (helper.isFunction()) {
                Class clazz = Class.forName(helper.getClassName());
                ExcelValidatorResult validResult = (ExcelValidatorResult) clazz.getMethod(helper.getFunctionName(), String.class).invoke(new String[]{}, fieldContent);
                if (!validResult.getResult()) {
                    result = false;
                    sb.append("【").append(headName).append(":").append(fieldContent).append("】格式不正确;");
                    sb.append(validResult.getMessage());
                }
            }
        }




        if (Date.class.isAssignableFrom(helper.getClazz())) {
            // 验证日期
            if (ExcelValidatorUtil.isEffective(fieldContent)) {
                Boolean dateCheck = false;
                for (String format : helper.getFormat()) {
                    if (ExcelValidatorUtil.isValidDate(fieldContent, format)) {
                        dateCheck = true;
                        break;
                    }
                }
                if (!dateCheck) {
                    result = false;
                    sb.append("【").append(headName).append(":").append(fieldContent).append("】日期格式不正确").append("\n");
                }
            }
        } else if (Boolean.class.isAssignableFrom(helper.getClazz())) {
            // 验证Boolean
            if ((!fieldContent.equals(helper.getFalseName())
                    && !fieldContent.equals(helper.getTrueName()))) {
                result = false;
                sb.append("【").append(headName).append(":").append(fieldContent).append("】格式不正确:").append(helper.getFalseName()).append(helper.getTrueName()).append("\n");
            }
        } else if (Integer.class.isAssignableFrom(helper.getClazz())) {
            // 验证数字
            if (!ExcelValidatorUtil.IsNumber(fieldContent)) {
                result = false;
                sb.append("【").append(headName).append(":").append(fieldContent).append("】格式不正确,需填写数字\n");
            }
        } else if (BigDecimal.class.isAssignableFrom(helper.getClazz()) && null != helper.getScale()) {
            // 验证金额
            String regexp = "^[+-]?[0-9]+(.[0-9]{1," + (helper.getScale() != null ? helper.getScale() : 2)
                    + "})?$";
            if (!(ExcelValidatorUtil.match(regexp, fieldContent))) {
                result = false;
                sb.append("【").append(headName).append(":").append(fieldContent).append("】精度不正确,精度为").append(helper.getScale()).append("\n");
            }
        }

        excelValidatorResult.setResult(result);
        excelValidatorResult.setMessage(sb.toString());
        return excelValidatorResult;
    }

    public static Boolean checkRow(Map<String, String> content){
        //如果content中都为空，则跳过
        Boolean cellIsNull = true;
        for (Map.Entry<String, String> m : content.entrySet()) {
            if (ExcelValidatorUtil.isEffective(m.getValue())) {
                cellIsNull = false;
            }
        }
        return cellIsNull;
    }
}
