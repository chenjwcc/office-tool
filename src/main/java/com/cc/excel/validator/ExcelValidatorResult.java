package com.cc.excel.validator;

/**
 * ClassName 验证结果实体
 * Author chenjw
 **/
public class ExcelValidatorResult {

    /**
     * 验证结果
     */
    private Boolean result;

    /**
     * 错误信息
     */
    private String message;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        if (result) {
            return "【验证结果】：验证成功";
        } else {
            return "【验证结果】：验证失败;【提示信息】："+ message;
        }


    }
}
