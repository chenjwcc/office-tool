package com.cc.excel.validator;

import com.cc.excel.annotaion.*;
import com.cc.excel.model.ExcelHelper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class ExcelValidatorUtil {

    /**
     * 是否有效，是否为空
     * @param paramString
     * @return
     */
    public static boolean isEffective(String paramString) {
        return (paramString != null) && (!"".equals(paramString)) && (!" ".equals(paramString))
                && (!"null".equals(paramString)) && (!"\n".equals(paramString));
    }

    /**
     * 是否是数字
     * @param paramString
     * @return
     */
    public static boolean IsNumber(String paramString) {
        if (paramString == null)
            return false;
        return match("-?[0-9]*$", paramString);
    }

    /**
     * 是否是日期
     * @param str
     * @param formatStr
     * @return
     */
    public static boolean isValidDate(String str, String formatStr) {
        boolean convertSuccess = true;
        // 指定日期格式为四位年/两位月份/两位日期，注意yyyy/MM/dd区分大小写；
        SimpleDateFormat format = new SimpleDateFormat(formatStr);
        try {
            // 设置lenient为false.
            // 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
            format.setLenient(false);
            format.parse(str);
        } catch (ParseException e) {
            // e.printStackTrace();
            // 如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            convertSuccess = false;
        }
        return convertSuccess;
    }

    /**
     * 匹配
     * @param paramString1
     * @param paramString2
     * @return
     */
    public static boolean match(String paramString1, String paramString2) {
        if (paramString2 == null) {
            return false;
        }
        return Pattern.compile(paramString1).matcher(paramString2).matches();
    }

    /**
     * 获取转换实体类的字段名称及排序
     * 
     * @param className
     * @return
     */
    public static Map<Integer, String> getBeanFieldNameAndSort(Class<?> className) {
        Map<Integer, String> map = new LinkedHashMap<>();
        Field[] fields = className.getDeclaredFields();
        Integer sort = Integer.MAX_VALUE;
        for (Field field : fields) {
            if (!field.getName().equals("serialVersionUID")) {
                if (field.isAnnotationPresent(ExcelField.class)) {
                    ExcelField fd = field.getAnnotation(ExcelField.class);
                    //启用访问安全检查
                    if (!field.isAccessible())
                        field.setAccessible(true);
                    map.put((0 == fd.sort())? sort-- : fd.sort(), fd.name());
                }
            }
        }
        return map;
    }

    /**
     * 读取注解值、字段名及字段的类型
     * 
     * @param className
     * @return
     * @throws Exception
     */
    public static Map<String, ExcelHelper> loadExcelAnnotationFieldVlaue(Class<?> className) throws Exception {
        Map<String, ExcelHelper> temp = new HashMap<String, ExcelHelper>();
        Field[] fields = className.getDeclaredFields();
        for (Field field : fields) {
            if (!field.getName().equals("serialVersionUID")) {
                //启用访问安全检查
                if (!field.isAccessible())
                    field.setAccessible(true);

                ExcelHelper helper = new ExcelHelper();
                //获取字段类型
                Type type = field.getGenericType();
                if (type instanceof Class<?>) {
                    Class<?> cls = (Class<?>) type;
                    helper.setClazz(cls);
                }

                //获取字段名称
                helper.setFieldName(field.getName());

                //获取字段注解
                Annotation[] ans = field.getAnnotations();
                for (Annotation annotation : ans) {
                    if (annotation.annotationType().equals(ExcelField.class)) {
                        //ExcelField注解
                        ExcelField fd = field.getAnnotation(ExcelField.class);
                        helper.setSort(fd.sort());
                        helper.setName(fd.name());
                        helper.setIsRequire(fd.isRequire());
                    } else if (annotation.annotationType().equals(ExcelBoolean.class)) {
                        //ExcelBoolean注解
                        ExcelBoolean fd = field.getAnnotation(ExcelBoolean.class);
                        helper.setFalseName(fd.False().toString());
                        helper.setTrueName(fd.True().toString());
                    } else if (annotation.annotationType().equals(ExcelDate.class)) {
                        //ExcelDate注解
                        ExcelDate fd = field.getAnnotation(ExcelDate.class);
                        helper.setFormat(fd.format());
                    } else if (annotation.annotationType().equals(ExcelValid.class)) {
                        //ExcelValid注解
                        ExcelValid fd = field.getAnnotation(ExcelValid.class);
                        helper.setRegexp(fd.regexp());
                        helper.setExample(fd.example());
                    } else if (annotation.annotationType().equals(ExcelBigDecimal.class)) {
                        //ExcelBigDecimal注解
                        ExcelBigDecimal fd = field.getAnnotation(ExcelBigDecimal.class);
                        helper.setScale(fd.scale());
                    } else if (annotation.annotationType().equals(ExcelCustomData.class)) {
                        //根据类名和获取
                        ExcelCustomData fd = field.getAnnotation(ExcelCustomData.class);
                        if (null != fd.customData() && fd.customData().length > 0) {
                            helper.setCustomData(fd.customData());
                            helper.setMessage(fd.message());
                        }else if (isEffective(fd.className()) && isEffective(fd.functionName())) {
                            Class clazz = Class.forName(fd.className());
                            String[] customData = (String[]) clazz.getMethod(fd.functionName()).invoke(new String[]{});
                            helper.setCustomData(customData);
                            helper.setMessage(fd.message());
                        }
                    } else if (annotation.annotationType().equals(ExcelFunction.class)){
                        ExcelFunction fd = field.getAnnotation(ExcelFunction.class);
                        helper.setIsFunction(true);
                        helper.setClassName(fd.className());
                        helper.setFunctionName(fd.functionName());
                    }
                }
                temp.put(helper.getFieldName(), helper);
            }
        }

        Map<String, ExcelHelper> map = new HashMap<String, ExcelHelper>();
        for (Map.Entry<String, ExcelHelper> m : temp.entrySet()) {
            map.put(m.getValue().getName(), m.getValue());
        }
        return map;
    }
}
