package com.cc.excel.model;

import java.io.Serializable;

public class ExcelHelper implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 字段名称
	 */
	private String fieldName;
	
	/**
	 * 字段类型
	 */
	private Class<?> clazz;
	
	/** ExcelField 注解属性*/
	private String name; //名称
	private int sort;  //排序
	private boolean isRequire; //是否为空
	
	/** ExcelBoolean 注解属性*/
	private String falseName;  //false
	private String trueName;   //true
	
	/** ExcelValid 注解属性*/
	private String regexp; //正则表达式
	private String example; //示例
	
	/** ExcelDate 注解属性*/
	private String[] format; //日期格式
	
	/** ExcelBigDecimal 注解属性*/
	private Integer scale; //精度


	/** ExcelCustomData 注解属性*/
	private String[] customData; //类名
	private String message; //提示语

	/** ExcelFunction 注解属性*/
	private Boolean isFunction = Boolean.FALSE; //是否自定义验证方法
	private String className; //类名
	private String functionName; //方法名

	public String[] getCustomData() {
		return customData;
	}

	public void setCustomData(String[] customData) {
		this.customData = customData;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public boolean isRequire() {
		return isRequire;
	}

	public void setIsRequire(boolean isRequire) {
		this.isRequire = isRequire;
	}

	public String getRegexp() {
		return regexp;
	}

	public void setRegexp(String regexp) {
		this.regexp = regexp;
	}

	public String[] getFormat() {
		return format;
	}

	public void setFormat(String[] format) {
		this.format = format;
	}

	public String getFalseName() {
		return falseName;
	}

	public void setFalseName(String falseName) {
		this.falseName = falseName;
	}

	public String getTrueName() {
		return trueName;
	}

	public void setTrueName(String trueName) {
		this.trueName = trueName;
	}

	public Integer getScale() {
		return scale;
	}

	public void setScale(Integer scale) {
		this.scale = scale;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Boolean isFunction() {
		return isFunction;
	}

	public void setIsFunction(Boolean function) {
		isFunction = function;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}


}
