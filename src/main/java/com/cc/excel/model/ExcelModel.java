package com.cc.excel.model;

import java.util.List;
import java.util.Map;

/**
 * ClassName ExcelModel
 * Author chenjw
 **/
public class ExcelModel {

    /**
     * 表头
     */
    private Map<Integer, String> header;

    /**
     * 内容
     */
    private List<Map<String, String>> content;

    public Map<Integer, String> getHeader() {
        return header;
    }

    public void setHeader(Map<Integer, String> header) {
        this.header = header;
    }

    public List<Map<String, String>> getContent() {
        return content;
    }

    public void setContent(List<Map<String, String>> content) {
        this.content = content;
    }
}
