package com.cc.excel.model;

public enum ExcelEnum {

	TRUE("是"), FALSE("否");

	public String name;

	ExcelEnum(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
