package com.cc.excel.model;

/**
 * ClassName ExcelToolEnum
 * Author chenjw
 **/
public enum ExcelToolEnum {

    JXL("JXL"),
    JXLS("JXLS"),
    POI("POI");

    ExcelToolEnum(String name) {
        this.name = name;
    }

    public String name;

    @Override
    public String toString() {
        return this.name;
    }


}
