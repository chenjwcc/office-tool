package com.cc.excel.transform;

import com.cc.excel.model.ExcelHelper;
import com.cc.excel.validator.ExcelValidator;
import com.cc.excel.validator.ExcelValidatorUtil;


import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * ClassName Transform
 * Author chenjw
 **/
public class ExcelTransform {

    /**
     * 将Excel的内容转换成实体类
     * 
     * @param clazz
     * @return
     * @throws Exception
     */
    public static <T> List<T> excelToEntity(List<Map<String, String>> contents, Class<T> clazz) throws Exception {
        List<T> list = new ArrayList<T>();
        Map<String, ExcelHelper> map = ExcelValidatorUtil.loadExcelAnnotationFieldVlaue(clazz);

        for (Map<String, String> content : contents) {
            T t = (T) clazz.newInstance();
            if (content.isEmpty()) {
                continue;
            }
            if(ExcelValidator.checkRow(content)){continue;}
            for (Map.Entry<String, String> m : content.entrySet()) {

                //根据单元格表头名称获取指定excelhelper
                ExcelHelper helper = map.get(m.getKey());

                Field f = t.getClass().getDeclaredField(helper.getFieldName());
                if (!f.isAccessible())
                    f.setAccessible(true);
                if (!ExcelValidatorUtil.isEffective(m.getValue())) {
                    continue;
                }
                if (Date.class.isAssignableFrom(helper.getClazz())) {
                    if (ExcelValidatorUtil.isEffective(m.getValue())) {
                        for (String format : helper.getFormat()) {
                            if (ExcelValidatorUtil.isValidDate(m.getValue(), format)) {
                                f.set(t, new SimpleDateFormat(format).parse(m.getValue().toString()));
                                break;
                            }
                        }
                    } else {
                        f.set(t, null);
                    }
                } else if (BigDecimal.class.isAssignableFrom(helper.getClazz())) {
                        f.set(t, BigDecimal.valueOf(Double.valueOf(m.getValue().toString()))
                                .setScale(helper.getScale() != null ? helper.getScale() : 2, BigDecimal.ROUND_HALF_UP));
                } else if (Boolean.class.isAssignableFrom(helper.getClazz())) {
                    f.set(t, m.getValue().toString().equals(helper.getTrueName()) ? true : false);
                } else if (String.class.isAssignableFrom(helper.getClazz())) {
                    f.set(t, m.getValue().toString());
                } else if (Integer.class.isAssignableFrom(helper.getClazz())) {
                    f.set(t, Integer.valueOf(m.getValue().toString()));
                }
            }
            list.add(t);
        }
        return list;
    }
}
