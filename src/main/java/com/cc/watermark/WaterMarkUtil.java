package com.cc.watermark;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * ClassName WaterMarkUtil
 * Author chenjw
 **/
public class WaterMarkUtil {

    /**
     * 生成水印
     * @param waterMarkModel
     * @return
     * @throws IOException
     */
    public static BufferedImage  createWaterMark(WaterMarkModel waterMarkModel) throws IOException {

        Integer width = waterMarkModel.getImageWidth();
        Integer height = waterMarkModel.getImageHeight();
        // 获取bufferedImage对象
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        // 处理背景色，设置为 白色
        int minx = bi.getMinX();
        int miny = bi.getMinY();
        for (int i = minx; i < width; i++) {
            for (int j = miny; j < height; j++) {
                bi.setRGB(i, j, 0xffffff);
            }
        }
		//Font font = new Font("华文细黑", Font.ITALIC, 20);
        // 获取Graphics2d对象
        Graphics2D g2d = bi.createGraphics();
        // 设置字体颜色为灰色
        g2d.setColor(waterMarkModel.getColor());
        // 设置图片的属性
        g2d.setStroke(new BasicStroke(1));
        // 设置字体
        Font font = waterMarkModel.getFont();
        g2d.setFont(font);
        // 设置字体倾斜度
        g2d.rotate(Math.toRadians(-10), (double) bi.getWidth() / 2, (double) bi.getHeight() / 2);
        //设置字体平滑
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // 写入水印文字 原定高度过小，所以累计写水印，增加高度
        for (int i = 1; i < 4; i++) {
            g2d.drawString(waterMarkModel.getWatermark(), 100, 300*i);
        }
        // 设置透明度
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
        // 释放对象
        g2d.dispose();

        return bi;
    }

}
