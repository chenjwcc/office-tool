package com.cc.watermark;

import java.awt.*;

/**
 * ClassName WaterMarkModel
 * Author chenjw
 **/
public class WaterMarkModel {

    /**
     * 水印文字
     */
    private String watermark;

    /**
     * 水印图片宽度
     */
    private Integer imageWidth;

    /**
     * 水印图片高度
     */
    private Integer imageHeight;

    /**
     * 颜色
     */
    private Color color;


    /**
     * 字体
     */
    private Font font;

    public WaterMarkModel(){
        this.setWatermark("内部资料");
        this.imageWidth = 600;
        this.imageHeight = 1020;
        this.color = Color.LIGHT_GRAY;
        this.font = new Font("黑体", Font.PLAIN, 50);
    }

    public Integer getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
    }

    public Integer getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public String getWatermark() {
        return watermark;
    }

    public void setWatermark(String watermark) {
        this.watermark = watermark;
    }




}
