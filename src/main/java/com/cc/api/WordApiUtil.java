package com.cc.api;

import com.cc.watermark.WaterMarkModel;
import com.cc.word.factory.WordToolFactory;
import com.cc.word.model.WordToolEnum;

import java.io.File;

/**
 * ClassName WordApiUtil
 * Author chenjw
 **/
public class WordApiUtil {

    /**
     * 生成文字水印
     * @param file
     * @param outFile
     * @param waterMark
     * @return
     * @throws Exception
     */
    public static Boolean addWaterMark(File file, File outFile, String waterMark) throws Exception {
        WaterMarkModel waterMarkModel = new WaterMarkModel();
        waterMarkModel.setWatermark(waterMark);
        return WordToolFactory.build(WordToolEnum.POI).addWaterMark(file, outFile, waterMarkModel);
    }
}
