package com.cc.api;

import com.cc.excel.factory.ExcelToolFactory;
import com.cc.excel.model.ExcelModel;
import com.cc.excel.model.ExcelToolEnum;
import com.cc.excel.transform.ExcelTransform;
import com.cc.excel.validator.ExcelValidator;
import com.cc.excel.validator.ExcelValidatorResult;
import com.cc.watermark.WaterMarkModel;

import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * ClassName ExcelApiUtil
 * Author chenjw
 **/
public class ExcelApiUtil {


    /**
     * 检验表头
     * 	表头默认为第1个sheet页，第1行，表头校验
     * @param file
     * @param clazz
     * @return
     * @throws Exception
     */
    public static ExcelValidatorResult checkExcelHeader(File file, Class<?> clazz) throws Exception {
        return checkExcelTitleAndSort(file, clazz, autoToolEnum(file), 0, 0, false);
    };

    /**
     * 检验表头
     * 	表头默认为第1个sheet页，第1行，表头校验
     * @param file
     * @param clazz
     * @return
     * @throws Exception
     */
    public static ExcelValidatorResult checkExcelHeader(File file, Class<?> clazz, Boolean isCheckSort) throws Exception {
        return checkExcelTitleAndSort(file, clazz, autoToolEnum(file), 0, 0, isCheckSort);
    };

    /**
     * 校验表头
     * @param file
     * @param clazz
     * @param sheetNumber sheet页序号
     * @param rowNumber  表头所在行数
     * @param isCheckSort 是否名称顺序都校验
     * @return
     * @throws Exception
     */
    public static ExcelValidatorResult checkExcelHeader(File file, Class<?> clazz, Integer sheetNumber, Integer rowNumber, Boolean isCheckSort) throws Exception {
        return checkExcelTitleAndSort(file, clazz, autoToolEnum(file), sheetNumber, rowNumber, isCheckSort);
    }

    /**
     * 校验表头名称与序号
     * @param file
     * @param clazz
     * @param sheetNumber
     * @param rowNumber
     * @param isCheckSort
     * @return
     * @throws Exception
     */
    protected static ExcelValidatorResult checkExcelTitleAndSort(File file, Class<?> clazz, ExcelToolEnum toolEnum, Integer sheetNumber, Integer rowNumber, Boolean isCheckSort) throws Exception {
        ExcelModel model = ExcelToolFactory.build(toolEnum).readExcel(file, clazz, sheetNumber, rowNumber);
        ExcelValidatorResult excelValidatorResult =  ExcelValidator.equalsTitleAndSort(model.getHeader(),clazz, isCheckSort);
        return excelValidatorResult;
    }

    /**
     * 验证excel文件的内容是否正确
     * @param file
     * @param clazz
     * @return
     */
    public static ExcelValidatorResult checkExcelContent(File file, Class<?> clazz) throws Exception {
        return checkExcelContent(file, 0, 0, clazz, autoToolEnum(file));
    }

    /**
     * 验证Excel文件的内容格式是否正确
     * 
     * @param file
     * @return
     */
    public static ExcelValidatorResult checkExcelContent(File file, Integer sheetNumber, Integer rowNumber, Class<?> clazz) throws Exception{
        return checkExcelContent(file, sheetNumber, rowNumber, clazz, autoToolEnum(file));
    }

    /**
     * 验证Excel文件的内容格式是否正确
     * 
     * @param file
     * @return
     */
    protected static ExcelValidatorResult checkExcelContent(File file, Integer sheetNumber, Integer rowNumber, Class<?> clazz, ExcelToolEnum toolEnum) throws Exception{
        ExcelModel model = ExcelToolFactory.build(toolEnum).readExcel(file, clazz, sheetNumber, rowNumber);
        ExcelValidatorResult excelValidatorResult = ExcelValidator.validExcelContent(model.getContent(), rowNumber, clazz);
        return excelValidatorResult;
    }

    /**
     * excel转化为实体
     * @param <T>
     * @param file
     * @param clazz
     * @return
     */
    public static <T> List<T> excelToEntity(File file, Class<T> clazz) throws Exception {
        return excelToEntity(file, 0, 0, clazz, autoToolEnum(file));
    }

    /**
     * excel转化为实体
     * @param file
     * @param sheetNumber
     *
     *
     * @param rowNumber
     * @param clazz
     * @param <T>
     * @return
     * @throws Exception
     */
    public static <T> List<T> excelToEntity(File file, Integer sheetNumber, Integer rowNumber, Class<T> clazz) throws Exception{
        return excelToEntity(file, sheetNumber, rowNumber, clazz, autoToolEnum(file));
    }

    /**
     * excel转化为实体
     * @param file
     * @param sheetNumber
     * @param rowNumber
     */
    protected static <T> List<T> excelToEntity(File file, Integer sheetNumber, Integer rowNumber, Class<T> clazz, ExcelToolEnum toolEnum) throws Exception {
        ExcelModel model = ExcelToolFactory.build(toolEnum).readExcel(file, clazz, sheetNumber, rowNumber);
        List<T> list = ExcelTransform.excelToEntity(model.getContent(), clazz);
        return list;
    }

    /**
     * 添加水印
     * @param file 输入文件
     * @param outFile 输出文件
     * @param waterMark 水印
     * @throws Exception
     */
    public static Boolean addWaterMark(File file, File outFile, String waterMark) throws Exception {
        WaterMarkModel waterMarkModel = new WaterMarkModel();
        waterMarkModel.setWatermark(waterMark);
        return addWaterMark(file, outFile, waterMarkModel, autoToolEnum(file));
    }

    /**
     * 添加水印
     * @param file 输入文件
     * @param outFile 输出文件
     * @param waterMark 水印
     * @param color 颜色
     * @throws Exception
     */
    public static Boolean addWaterMark(File file, File outFile, String waterMark, Color color) throws Exception {
        WaterMarkModel waterMarkModel = new WaterMarkModel();
        waterMarkModel.setWatermark(waterMark);
        waterMarkModel.setColor(color);
        return addWaterMark(file, outFile, waterMarkModel, autoToolEnum(file));
    }

    /**
     * 添加水印
     * @param file 输入文件
     * @param outFile 输出文件
     * @param waterMark 水印
     * @param color 颜色
     * @param color 字体
     * @throws Exception
     */
    public static Boolean addWaterMark(File file, File outFile, String waterMark, Color color, Font font) throws Exception {
        WaterMarkModel waterMarkModel = new WaterMarkModel();
        waterMarkModel.setWatermark(waterMark);
        waterMarkModel.setColor(color);
        waterMarkModel.setFont(font);
        return addWaterMark(file, outFile, waterMarkModel, autoToolEnum(file));
    }

    /**
     * 添加水印
     * @param file
     * @param outFile
     * @param waterMarkModel
     * @param toolEnum
     * @throws Exception
     */
    public static Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel, ExcelToolEnum toolEnum) throws Exception {
        return ExcelToolFactory.build(toolEnum).addWaterMark(file, outFile, waterMarkModel);
    }

    /**
     * 根据模板导出excel
     * @param file 模板
     * @param outFile 输出文件
     * @param map 填充数据
     * @throws Exception
     */
    public static Boolean exportExcel(File file, File outFile, Map<String, Object> map) throws Exception {
        return ExcelToolFactory.build(ExcelToolEnum.JXLS).exportExcel(file, outFile, map);
    }

    /**
     * 根据文件类型自动匹配解析工具
     * @param file
     * @return
     * @throws Exception
     */
    private static ExcelToolEnum autoToolEnum(File file) throws Exception {
        String name = file.getName();
        String suffix = name.substring(name.lastIndexOf("."));
        if (".xls".equals(suffix)){
            return ExcelToolEnum.JXL;
        } else if (".xlsx".equals(suffix)){
            return ExcelToolEnum.POI;
        } else {
            throw new Exception("文件类型有误");
        }
    }
}
