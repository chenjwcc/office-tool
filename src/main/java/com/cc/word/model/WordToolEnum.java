package com.cc.word.model;

public enum WordToolEnum {

    POI("POI");

    WordToolEnum(String name) {
        this.name = name;
    }

    public String name;

    @Override
    public String toString() {
        return this.name;
    }
}
