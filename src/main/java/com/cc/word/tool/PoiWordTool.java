package com.cc.word.tool;

import com.cc.watermark.WaterMarkModel;
import com.cc.word.factory.WordAbstractTool;
import com.cc.word.util.XWPFHeaderFooterPolicyExtend;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;

/**
 * ClassName PoiWordTool
 * Author chenjw
 **/
public class PoiWordTool extends WordAbstractTool {

    @Override
    public Boolean addWaterMark(File inputFile, File outFile, WaterMarkModel waterMarkModel) throws Exception {

        String markStr = waterMarkModel.getWatermark();

        String extString = inputFile.getName().substring(inputFile.getName().lastIndexOf("."));
        if (".doc".equals(extString)) {
            throw new Exception("不支持doc文件");
        }

        //2003doc 用HWPFDocument  ； 2007doc 用 XWPFDocument
        XWPFDocument doc = null;
        try {
            // 延迟解析比率
            ZipSecureFile.setMinInflateRatio(-1.0d);
            doc = new XWPFDocument(new FileInputStream(inputFile));
        } catch (FileNotFoundException e) {
            throw new Exception("源文件不存在:" + e.getMessage());
        } catch (IOException e) {
            throw new Exception("读取源文件IO异常:" + e.getMessage());
        } catch (Exception e) {
            throw new Exception("不支持的文档格式:" + e.getMessage());
        }

        XWPFHeaderFooterPolicyExtend extend = new XWPFHeaderFooterPolicyExtend(doc);

        //添加水印
        extend.createWatermark(markStr);
        OutputStream os = null;
        try {
            os = new FileOutputStream(outFile);
            doc.write(os);
        } catch (FileNotFoundException e) {
            throw new Exception("创建输出文件失败：" + e.getMessage());
        } catch (Exception e) {
            throw new Exception("添加文档水印失败:" + e.getMessage());
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (doc != null) {
                try {
                    doc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

}
