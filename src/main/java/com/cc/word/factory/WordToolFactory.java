package com.cc.word.factory;

import com.cc.word.model.WordToolEnum;
import com.cc.word.tool.PoiWordTool;

/**
 * ClassName WordToolFactory
 * Author chenjw
 **/
public class WordToolFactory {

    public static WordAbstractTool build(WordToolEnum toolType){
        return new PoiWordTool();

    }
}
