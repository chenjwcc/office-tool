package com.cc.word.factory;

import com.cc.watermark.WaterMarkModel;

import java.io.File;

/**
 * ClassName Word抽象工具类
 * Author chenjw
 **/
public abstract class WordAbstractTool {

    /**
     * 生成水印
     * @param file
     * @param outFile
     * @param waterMarkModel
     * @return
     */
    public abstract Boolean addWaterMark(File file, File outFile, WaterMarkModel waterMarkModel) throws Exception;
}
